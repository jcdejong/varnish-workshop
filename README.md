Varnish Playground
==================

12.1.2015, David Buchmann <david@liip.ch>

This repository is used for the Varnish workshop i do.

Please contact me at david@liip.ch if you are interested in doing a workshop
about caching and Varnish.

Setup
-----

### Preconditions

This is setting up a vagrant box. You will need to have Vagrant installed in
version 1.1 or later.

If you are not familiar with vagrant, please see https://www.vagrantup.com/

### Installation

1. Clone repository
2. Run `vagrant up`
3. Point your browser to http://varnish.lo for varnish, and http://varnish.lo:8080 for direct access to Apache.

### Troubleshooting

If you do not get a page on http://varnish.lo, check

* Did the `vagrant up` command report any errors?
* Can you ssh into the machine with `vagrant ssh`? If not, you need to look at
  the vagrant output. Starting the box with the visual VirtualBox client might
  give additional information.
* Inside the box, check if `curl vagrant.lo` works. If not, `sudo service varnish restart`.
  Check if `curl vagrant.lo:8080` works. If not, `sudo service apache2 restart`.
  If both say OK, things should be fine. If not, call for help ;-)

Web
---

Vagrant installs apache pointing to the web/ folder. There are some demo files
and folders, and the exercises and solutions folder. I recommend *not* to look
into solutions before doing the workshop.

Varnish
-------

Vagrant links the `default.vcl` file to config/varnish/dev.vcl. Have a look at those files.

License and Usage
-----------------

This material is intended for the workshop. Please refrain from making it
publicly available (sharing within your company is OK).

Be warned that the PHP code is intentionally kept minimal to focus on the
demonstrated features. Apart from the caching concepts, it may not reflect best
practices. Particularly the raw usage of $_SERVER and $_POST values would be a
security issue.
