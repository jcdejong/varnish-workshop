<?php

/*
 * Request this page with a modification time check:
 *
 * curl -H "If-Modified-Since: <last timestamp>" -sD - varnish.lo/last-modified.php
 *
 * Note: The timestamp must not be in the future or you will see 200 and content instead of 304.
 */

header('Cache-Control: s-maxage=300');

$data = date("Y-m-d H:i:s");

// note: gmdate('r') outputs the timezone as +0000 instead of GMT, making varnish 3 unhappy.
header('Last-Modified: ' . gmdate('D, j M Y H:i:00 T'));

echo $data . "\n";
