<?php

/*
 * Request this page and see a cache hit even though s-maxage is 0
 *
 * curl -sD - varnish.lo/custom-ttl.php
 */

header('Cache-Control: public, s-maxage=0, max-age=0');
header('X-Reverse-Proxy-TTL: 30');

echo date("Y-m-d H:i:s") . "\n";
