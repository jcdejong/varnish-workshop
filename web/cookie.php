<?php

/*
 * Request this page and see that it is not cached even though varnish is told to cache it.
 *
 * curl -sD - varnish.lo/cookie.php
 */

header('Cache-Control: public, s-maxage=3600, max-age=3600');

setcookie('boom', date("Y-m-d_H-i-s"));

echo date("Y-m-d H:i:s") . "\n";
