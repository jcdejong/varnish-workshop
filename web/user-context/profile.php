<?php
session_start();

header('Vary: Cookie');
header('Cache-Control: max-age=0');
header('X-Reverse-Proxy-TTL: 3600');

$body = 'Hi '.$_SESSION['username'];

require('template.php');
