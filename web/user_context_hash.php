<?php
/*
 * This file is used for the user-context demo
 */

if ('application/vnd.fos.user-context-hash' !== strtolower($_SERVER['HTTP_ACCEPT'])) {
    // 406 Not acceptable in case of an incorrect accept header
    header('HTTP/1.1 406');
    die;
}

require('../vendor/autoload.php');
use FOS\HttpCache\UserContext\HashGenerator;
use FOS\HttpCache\UserContext\ContextProviderInterface;
use FOS\HttpCache\UserContext\UserContext;

session_start();

class RoleProvider implements ContextProviderInterface
{
    public function updateUserContext(UserContext $userContext)
    {
        $userContext->addParameter('role', isset($_SESSION['role']) ? $_SESSION['role'] : 'anonymous');
    }
}

$hashGenerator = new HashGenerator(array(
    new RoleProvider(),
));
$hash = $hashGenerator->generateHash();

header(sprintf('X-User-Context-Hash: %s', $hash));
header('Content-Type: application/vnd.fos.user-context-hash');
header('Cache-Control: max-age=3600');
header('Vary: cookie, authorization');
